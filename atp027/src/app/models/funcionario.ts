export interface Funcionario {
    id:number,
    nome:string,
    sobrenome:string,
    salario:number
    cargo:string
}
