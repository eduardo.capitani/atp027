import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastroFuncionarioComponent } from './componentts/cadastrar-funcionario/cadastro-funcionario/cadastro-funcionario.component'
import { ListarFuncionarioComponent } from './componentts/listar-funcionario/listar-funcionario.component';

const routes: Routes = [
  {path:'cadastrar', component: CadastroFuncionarioComponent},
  {path:'listar', component: ListarFuncionarioComponent},
  {path:'editar/:id', component: CadastroFuncionarioComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
