import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './componentts/app-root/app.component';
import { CadastroFuncionarioComponent } from './componentts/cadastrar-funcionario/cadastro-funcionario/cadastro-funcionario.component';
import { ListarFuncionarioComponent } from './componentts/listar-funcionario/listar-funcionario.component';

@NgModule({
  declarations: [
    AppComponent,
    CadastroFuncionarioComponent,
    ListarFuncionarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
