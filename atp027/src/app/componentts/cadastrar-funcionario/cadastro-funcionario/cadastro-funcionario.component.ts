import { Component, OnInit } from '@angular/core';
import { Funcionario } from 'src/app/models/funcionario';
import { FuncionarioStorageService } from 'src/app/services/funcionario-storage.service';

@Component({
  selector: 'app-cadastrar-funcionarios',
  templateUrl: './cadastro-funcionario.component.html',
  styleUrls: ['./cadastro-funcionario.component.css']
})
export class CadastroFuncionarioComponent implements OnInit {
  public funcionario:Funcionario
  private funcionarioStorage: FuncionarioStorageService;

  constructor() {
    this.funcionario = {
      id: 0,
      nome: '',
      sobrenome: '',
      cargo: '',
      salario: 0
    }
    this.funcionarioStorage = new FuncionarioStorageService();
   }

  ngOnInit(): void {
  }

  salvar(){
    this.funcionarioStorage.save(this.funcionario);
    this.limpar
  }

  limpar(){
    this.funcionario = {
      id: 0,
      nome: '',
      sobrenome: '',
      cargo: '',
      salario: 0
    }
  }
}
  
