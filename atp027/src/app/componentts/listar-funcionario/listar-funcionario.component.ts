import { Component, OnInit } from '@angular/core';
import { Funcionario } from 'src/app/models/funcionario';
import { FuncionarioStorageService } from 'src/app/services/funcionario-storage.service';

@Component({
  selector: 'app-listar-funcionarios',
  templateUrl: './listar-funcionario.component.html',
  styleUrls: ['./listar-funcionario.component.css']
})
export class ListarFuncionarioComponent implements OnInit {
  public funcionarios: Funcionario[];
  private funcionariosStorage: FuncionarioStorageService;

  constructor() { 
    this.funcionariosStorage = new FuncionarioStorageService();
    this.funcionarios = this.funcionariosStorage.readAll();
  }


  ngOnInit(): void {
    let nome = 'Lista de pessoas'
  }

}
