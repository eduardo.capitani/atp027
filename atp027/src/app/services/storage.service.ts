import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private storage:Storage;

  constructor() { 
    this.storage = window.localStorage;
  }

  get(key:string):any{
    return this.storage.getItem(key);

  }

  set(key:string, value:string){
    this.storage.setItem(key, value)
  }
}
