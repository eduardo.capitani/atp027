import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Funcionario } from '../models/funcionario';


@Injectable({
  providedIn: 'root'
})
export class FuncionarioServiceService {
  private enderecoApi = 'http://189.126.106.251:3000/person';

  constructor(private http:HttpClient) { }

  create(funcionario:Funcionario):Observable<string>{
    return this.http.post(`${this.enderecoApi}`, funcionario, {responseType:'text'});
  }
  readAll():Observable<Funcionario[]>{
    return this.http.get<Funcionario[]>(`${this.enderecoApi}`);
  }
  readById(id:number):Observable<Funcionario>{
    return this.http.get<Funcionario>(`${this.enderecoApi}/${id}`);
  }
  update(funcionario:Funcionario):Observable<string>{
    return this.http.put(`${this.enderecoApi}/${funcionario.id}`, funcionario, {responseType:'text'});
  }
  delete(id:number):Observable<string>{
    return this.http.delete(`${this.enderecoApi}/${id}`, {responseType:'text'});
  }
}

