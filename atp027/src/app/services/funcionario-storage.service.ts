import { Injectable } from '@angular/core';
import { Funcionario } from '../models/funcionario';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class FuncionarioStorageService extends StorageService{
  private listaFuncionarios:Funcionario[];
  private localStorageKey:string;
  private localStorageKeyId:string;
  private id:number;


  constructor() {
    super()
    this.localStorageKeyId = 'funcionariosId';
    this.id = this.getId();
    this.listaFuncionarios = this.readAll();
    this.localStorageKey ='funcionarios'
   }

   save(funcionario:Funcionario){
    this.generateId();
    funcionario.id = this.id;
    this.listaFuncionarios.push(funcionario)
    super.set(this.localStorageKey, JSON.stringify(this.listaFuncionarios));
   }

   readAll():Funcionario[]{
    this.listaFuncionarios = JSON.parse(super.get(this.localStorageKey))
    if(this.listaFuncionarios == null){
      this.listaFuncionarios = [];
    }
    return this.listaFuncionarios;
   }

   readById(id:number):Funcionario{
    let funcionario = this.listaFuncionarios.find(p=>p.id==id);
    if(funcionario == undefined){
      console.log('id nao encontrado na lista'); 
      funcionario = {id:0, nome:'', sobrenome:'', salario:0, cargo:''};
    }
    return funcionario;
  }

  editar(funcionario:Funcionario){
    let newList = this.generateNewList(funcionario.id);
    newList.push(funcionario);
    this.saveList(newList);
  }

  delete(id:number){
    this.saveList(this.generateNewList(id));
  }

  generateNewList(id:number):Funcionario[]{
    return this.listaFuncionarios.filter(p=>p.id != id);
  }

  generateId(){
    this.id = this.id +1;
    super.set(this.localStorageKeyId, JSON.stringify(this.id));
  }
  
  getId(){
    this.id = JSON.parse(super.get(this.localStorageKeyId));
    if(this.id == null){
      this.id = 0;
    }
    return this.id;
  }

  saveList(list:Funcionario[]){
    super.set(this.localStorageKey, JSON.stringify(list));
  }


}
