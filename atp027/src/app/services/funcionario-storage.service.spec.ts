import { TestBed } from '@angular/core/testing';

import { FuncionarioStorageService } from './funcionario-storage.service';

describe('FuncionarioStorageService', () => {
  let service: FuncionarioStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FuncionarioStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
